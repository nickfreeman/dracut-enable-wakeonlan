#!/usr/bin/bash

# called by dracut
check() {
  #check for ethtool
  require_binaries ethtool || return 1
  return 0
}

depends() {
  echo network
  return 0
}

install() {
  #some initialization

  inst "/usr/sbin/ethtool"
  inst_hook initqueue/online 50 "$moddir/wakeonlan.sh"
}
